<?php

class routing
{

    function __construct()
    {
        $this->routs = explode('/', $_REQUEST['path']); // Разделяем наш запрос
        $this->class = 'Gelios\\functions\\' . $this->routs[1] . 'Class';
        $this->params = explode('?', $this->routs[2]);
        $this->method = $this->params[0];
        $this->request = $_REQUEST;
        $this->json = json_encode($this->get_routs());
    }

    function get_routs()
    {
        $method = $this->method;
        return $this->class::$method($this->request);
    }
}