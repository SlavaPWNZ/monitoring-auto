<?php

namespace Gelios\classes;

use Gelios\geliosAPI;

class getUnitInfoClass extends geliosAPI
{
    public static $api_method = 'get_unit_info';
    public static $params = ["id_unit"];

    public static function getCoordinates($params)
    {
        $result = self::request(self::$api_method, self::parse_params($params));
        if (empty($result->error)) {
            if (!empty($result->lmsg)) {
                return (object)[
                    'id' => $result->id,
                    'name' => $result->name,
                    'lat' => $result->lmsg->lat,
                    'lon' => $result->lmsg->lon
                ];
            }
            return (object)['error' => 'no info about unit'];
        }
        return (object)['error' => $result->data_error];
    }
}