<?php

namespace Gelios\classes;

use Gelios\geliosAPI;

class createGeozoneGroupClass extends geliosAPI
{
    public static $api_method = 'create_geozone_group';
    public static $params = ['name'];

    public static function create($params)
    {
        $result = self::request(self::$api_method, self::parse_params($params));
        if (empty($result->error)) {
            return (object)$result;
        }
        return (object)['error' => $result->data_error];
    }
}