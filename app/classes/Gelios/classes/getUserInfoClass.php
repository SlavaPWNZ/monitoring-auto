<?php

namespace Gelios\classes;

use Gelios\geliosAPI;

class getUserInfoClass extends geliosAPI
{
    public static $api_method = 'get_user_info';
    public static $params = ["id"];

    public static function get($params)
    {
        $result = self::request(self::$api_method, self::parse_params($params));
        if (empty($result->error)) {
            return (object)$result;
        }
        return (object)['error' => $result->data_error];
    }
}