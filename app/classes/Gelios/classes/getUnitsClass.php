<?php

namespace Gelios\classes;

use Gelios\geliosAPI;

class getUnitsClass extends geliosAPI
{
    public static $api_method = 'get_units';
    public static $params = ["id_group", "short_details"];

    public static function get($params)
    {
        $units = [];
        $result = self::request(self::$api_method, self::parse_params($params + ["short_details" => 1]));
        if (empty($result->error)) {
            foreach ($result as $item) {
                $units[] = (object) ["id" => $item->id, "name" => $item->name];
            }
            return (object)$units;
        }
        return (object)['error' => $result->data_error];
    }
}