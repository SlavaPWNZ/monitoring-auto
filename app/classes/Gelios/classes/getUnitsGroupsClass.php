<?php

namespace Gelios\classes;

use Gelios\geliosAPI;

class getUnitsGroupsClass extends geliosAPI
{
    public static $api_method = 'get_units_groups';
    public static $params = ["id_user"];

    public static function get($params)
    {
        $result = self::request(self::$api_method, self::parse_params(["id_user" => parent::$gelios_user_id]));
        if (empty($result->error)) {
            return (object)$result;
        }
        return (object)['error' => $result->data_error];
    }
}