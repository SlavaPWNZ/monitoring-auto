<?php

namespace Gelios\classes;

use Gelios\geliosAPI;

class createGeozoneClass extends geliosAPI
{
    public static $api_method = 'create_geozone';
    public static $params = ['id_group', 'name', 'coords', 'description', 'perimeter', 'radius', 'docs_surface_area'];

    public static function create($params)
    {
        $result = self::request(self::$api_method, self::parse_params($params));
        if (empty($result->error)) {
            return (object)$result;
        }
        return (object)['error' => $result->data_error];
    }
}