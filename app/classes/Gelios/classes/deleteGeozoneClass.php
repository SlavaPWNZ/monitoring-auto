<?php

namespace Gelios\classes;

use Gelios\geliosAPI;

class deleteGeozoneClass extends geliosAPI
{
    public static $api_method = 'delete_geozone';
    public static $params = ['id_geozone'];

    public static function delete($params)
    {
        $result = self::request(self::$api_method, self::parse_params($params));
        if (empty($result->error)) {
            return (object)$result;
        }
        return (object)['error' => $result->data_error];
    }
}