<?php

namespace Gelios\classes;

use Gelios\geliosAPI;

class getListNewReportsClass extends geliosAPI
{
    public static $api_method = 'get_list_new_reports';
    public static $params = ["new_report_general_user"];

    public static function get($params)
    {
        $result = self::request(self::$api_method, self::parse_params($params));
        if (empty($result->error)) {
            foreach ($result->reports as $row) {
                if (isset($params['name']) && $row->name == $params['name']) {
                    return (object)[
                        'id' => $row->id,
                        'name' => $row->name
                    ];
                }
            }
            return (object)$result;
        }
        return (object)['error' => $result->data_error];
    }
}