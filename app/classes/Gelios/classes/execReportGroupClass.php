<?php

namespace Gelios\classes;

use Gelios\geliosAPI;

class execReportGroupClass extends geliosAPI
{
    public static $api_method = 'exec_report_group';
    public static $params = ['id_group', 'from', 'to', 'report_type', 'language', 'id_route', 'id_geozone'];

    public static function get($params)
    {
        $result = self::request(self::$api_method, self::parse_params($params));
        if (empty($result->error)) {
            return (object)$result;
        }
        return (object)['error' => $result->data_error];
    }

    public static function stats($params)
    {
        //$getReport = getListNewReportsClass::get(['name' => 'Statistics']);
        //$params["report_type"] = $getReport->id;
        $params["report_type"] = 3935;
        $stats = [];
        $result = self::get($params);
        $result = json_decode($result->scalar); // API BUG!
        if (empty($result->error) && !empty($result->tables[0]->data)) {
            foreach ($result->tables[0]->data as $car){
                if (isset($car->unit_id)){
                    $stats[$car->unit_id]['name'] = $car->object;
                    $stats[$car->unit_id]['distance'] = $car->distance;
                    $stats[$car->unit_id]['duration_mov'] = $car->duration_mov;
                }
            }
            return (object)$stats;
        }
        return [];
    }

    public static function worktime($params)
    {
        //$getReport = getListNewReportsClass::get(['name' => 'Trips']);
        //$params["report_type"] = $getReport->id;
        $params["report_type"] = 3929;
        $worktime = [];
        $cars = [];
        $result = self::get($params);
        $result = json_decode($result->scalar); // API BUG!
        if (empty($result->error) && !empty($result->tables[0]->data)) {
            foreach ($result->tables[0]->data as $trip){
                if (isset($trip->unit_id)){
                    $cars[$trip->object][] = $trip;
                }
            }
            foreach($cars as $k => $v){
                $worktime[$k]['time_start'] = $v[0]->time_start;
                $worktime[$k]['time_end'] = $v[count($v)-1]->time_end;
            }
            return (object)$worktime;
        }
        return [];
    }

    public static function geozone($params, $coords)
    {
        $result = createGeozoneClass::create([
            "id_group" => geliosAPI::$gelios_geozone_group,
            "radius" => 300,
            "coords" => $coords,
            "name" => 'ApiGeozone',
            "description" => 'ApiGeozone description EMPTY',
        ]); // Создали геозону
        $id_geozone = $result->id;


        $params += ["id_geozone" => $id_geozone];
        $params["report_type"] = 3136;
        $geozone = [];
        $result = self::get($params);
        $result = json_decode($result->scalar); // API BUG!
        if (empty($result->error) && !empty($result->tables[0]->data)) {
            foreach ($result->tables[0]->data as $car){
                if (isset($car->name) && $car->name){
                    $geozone[$car->object][] = [
                        'time_start' => $car->time_start,
                        'time_end' => $car->time_end,
                    ];
                }
            }
        }
        deleteGeozoneClass::delete(["id_geozone" => $id_geozone]); // Удалили геозону
        return (object)$geozone;
    }
}