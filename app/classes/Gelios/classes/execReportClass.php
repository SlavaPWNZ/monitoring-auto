<?php

namespace Gelios\classes;

use Gelios\geliosAPI;

class execReportClass extends geliosAPI
{
    public static $api_method = 'exec_report';
    public static $params = ['id_unit', 'from', 'to', 'report_type', 'language', 'id_group', 'id_geozone'];

    public static function get($params)
    {
        $result = self::request(self::$api_method, self::parse_params($params));
        if (empty($result->error)) {
            return (object)$result;
        }
        return (object)['error' => $result->data_error];
    }

    public static function stats($params)
    {
        //$getReport = getListNewReportsClass::get(['name' => 'Statistics']);
        //$params["report_type"] = $getReport->id;
        $params["report_type"] = 3935;
        $result = self::get($params);
        if (empty($result->error) && !empty($result->tables[0]->data)) {
            return (object)[
                'distance' => $result->tables[0]->data[0]->distance,
                'duration_mov' => $result->tables[0]->data[0]->duration_mov,
            ];
        }
        return (object)['distance' => 0, 'duration_mov' => 0];
    }

    public static function worktime($params)
    {
        //$getReport = getListNewReportsClass::get(['name' => 'Trips']);
        //$params["report_type"] = $getReport->id;
        $params["report_type"] = 3929;
        $result = self::get($params);
        if (empty($result->error) && !empty($result->tables[0]->data)) {
            return (object)[
                'time_start' => $result->tables[0]->data[0]->time_start,
                'time_end' => $result->tables[0]->data[count($result->tables[0]->data) - 1]->time_end,
            ];
        }
        return (object)['time_start' => 0, 'time_end' => 0];
    }

    public static function geozone($params, $coords)
    {
        $result = createGeozoneClass::create([
            "id_group" => geliosAPI::$gelios_geozone_group,
            "radius" => 300,
            "coords" => $coords,
            "name" => 'ApiGeozone',
            "description" => 'ApiGeozone description EMPTY',
        ]); // Создали геозону
        $id_geozone = $result->id;


        $params += ["id_geozone" => $id_geozone];
        $params["report_type"] = 3136;
        $geozone = [];
        $result = self::get($params);
        if (empty($result->error) && !empty($result->tables[0]->data)) {
            foreach ($result->tables[0]->data as $car){
                if (isset($car->name) && $car->name){
                    $geozone[] = [
                        'time_start' => $car->time_start,
                        'time_end' => $car->time_end,
                    ];
                }
            }
        }
        deleteGeozoneClass::delete(["id_geozone" => $id_geozone]); // Удалили геозону
        return (object)$geozone;
    }
}