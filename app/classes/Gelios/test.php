<?php

namespace Gelios;

use Gelios\classes\{createGeozoneClass,
    deleteGeozoneClass,
    execReportGroupClass,
    getUnitsClass,
    getUnitInfoClass,
    getListNewReportsClass,
    execReportClass,
    getUnitsGroupsClass,
    getUserInfoClass};

class test
{
    public function __construct()
    {
        echo '<pre>';
        //$this->testGetUnitsClass();
        //$this->testGetUnitInfoClass();
        //$this->testReports();
        //$this->testGetUserInfoClass();
        //$this->testGetUnitsGroupsClass();
        //$this->testStatsTable();
        //$this->testStatsTableGroup();
        //$this->testGetGeozone();
        $this->testStatsTableGroupByGeozone();
        echo '</pre>';
        exit();
    }

    public function testGetUnitsClass()
    {
        echo "<b>getUnitsClass | Test 1. Получаем список объектов (машин)</b><br>";
        $result = getUnitsClass::get([]);
        print_r($result);
        echo "<br><br><br>";
    }

    public function testGetUnitInfoClass()
    {
        echo "<b>getUnitInfoClass | Test 2. Получаем координаты объекта по id (106039)</b><br>";
        $params = [
            "id_unit" => 106039,
        ];
        $result = getUnitInfoClass::getCoordinates($params);
        print_r($result);
        echo "<br><br><br>";
    }

    public function testReports()
    {
        echo "<b>getListNewReportsClass | Test 3. Список всех доступных отчётов</b><br>";
        $result = getListNewReportsClass::get([]);
        print_r($result);
        echo "<br><br><br>";


        echo "<b>getListNewReportsClass | Test 4. Поиск id нужного отчёта по его названию (Statistics/Статистика)</b><br>";
        $result = getListNewReportsClass::get(['name' => 'Statistics']);
        print_r($result);
        echo "<br><br><br>";


        echo "<b>execReportClass | Test 5. Получение отчёта за интервал времени</b><br>";
        $params = [
            "id_unit" => 106039,
            "from" => 1586736000,
            "to" => 1586995200,
            "report_type" => 3935,
        ];
        $result = execReportClass::get($params);
        print_r($result);
        echo "<br><br><br>";


        echo "<b>execReportClass | Test 6. Сколько машина проехала км за интервал времени</b><br>";
        $params = [
            "id_unit" => 106039,
            "from" => 1586736000,
            "to" => 1586995200,
        ];
        $result = execReportClass::stats($params);
        print_r($result);
        echo "<br><br><br>";

        echo "<b>execReportClass | Test 7. Когда машина начала работать, когда закончила</b><br>";
        $params = [
            "id_unit" => 106039,
            "from" => 1586736000,
            "to" => 1586995200,
        ];
        $result = execReportClass::worktime($params);
        print_r($result);
        echo "<br><br><br>";
    }

    public function testStatsTable(){
        $start = microtime(true);
        $result = [];
        echo "Статистика за 15/04 - 16/04<br>";
        $cars = getUnitsClass::get([]);
        foreach ($cars as $car){
            $params = [
                "id_unit" => $car->id,
                "from" => 1586908800,
                "to" => 1586995200,
            ];
            $stats = execReportClass::stats($params);
            $worktime= execReportClass::worktime($params);
            $result[] = (object) array_merge((array) $car, (array) $stats, (array) $worktime);
        }
        print_r($result);
        echo "<br><br><br>";
        echo 'Время выполнения скрипта: '.round(microtime(true) - $start, 4).' сек.';
    }

    public function testStatsTableGroup(){
        $start = microtime(true);
        $result = [];
        echo "Статистика за 15/04 - 16/04<br>";
        $params = [
            "from" => 1586908800,
            "to" => 1586995200,
            "id_group" => geliosAPI::$gelios_main_group
        ];
        $stats = execReportGroupClass::stats($params);
        $worktime= execReportGroupClass::worktime($params);
        foreach ($stats as $k => $v){
            if (!empty($worktime->$k)){
                $result[$k] = (object) array_merge((array) $stats->$k, (array) $worktime->$k);
            }else{
                $result[$k] = (object) array_merge((array) $stats->$k, ['time_start' => 0, 'time_end' => 0]);
            }
        }

        print_r($result);
        echo "<br><br><br>";
        echo 'Время выполнения скрипта: '.round(microtime(true) - $start, 4).' сек.';
    }

    public function testGetUserInfoClass()
    {
        echo "<b>getUserInfoClass | Test 8. Получаем список пользователей</b><br>";
        $result = getUserInfoClass::get([]);
        print_r($result);
        echo "<br><br><br>";
    }

    public function testGetUnitsGroupsClass()
    {
        echo "<b>getUnitsGroupsClass | Test 9. Получаем список групп</b><br>";
        $result = getUnitsGroupsClass::get([]);
        print_r($result);
        echo "<br><br><br>";
    }

    public function testGetGeozone()
    {
        echo "<b>createGeozoneClass | Test 10. Создание геозоны</b><br>";
        $result = createGeozoneClass::create([
            "id_group" => geliosAPI::$gelios_geozone_group,
            "radius" => 300,
            "coords" => "45.00173, 31.23794",
            "name" => 'test',
            "description" => 'test desc',
        ]); // Создали геозону
        print_r($result);
        echo "<br><br><br>";


        echo "<b>deleteGeozoneClass | Test 11. Удаление геозоны</b><br>";
        $result = deleteGeozoneClass::delete([
            "id_geozone" => $result->id,
        ]);
        print_r($result);
        echo "<br><br><br>";
    }


    public function testStatsTableGroupByGeozone(){
        $start = microtime(true);
        echo "Статистика за 20/04 - 21/04 по геоточке. Пересечение МКАД и шоссе Энтузиастов.<br>";
        $params = [
            "from" => 1587340800,
            "to" => 1587427200,
            "id_group" => geliosAPI::$gelios_main_group,
        ];
        $geozone = execReportGroupClass::geozone($params, "55.779876, 37.842464");
        print_r($geozone);
        echo "<br><br><br>";
        echo 'Время выполнения скрипта: '.round(microtime(true) - $start, 4).' сек.';
    }
}