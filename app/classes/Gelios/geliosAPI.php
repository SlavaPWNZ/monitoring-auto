<?php

namespace Gelios;

class geliosAPI
{
    private static $gelios_login = '93218-2';
    private static $gelios_pass = 'd8cj4f';
    private static $gelios_url = 'https://admin.geliospro.com/sdk/';
    public static $gelios_user_id = 41244; // getUserInfoClass
    public static $gelios_main_group = 50752; // getUnitsGroupsClass
    public static $gelios_geozone_group = 211286; // createGeozoneGroupClass


    final protected static function request($method, $params)
    {
        $get = $method . '&params=' . $params;
        $result = self::request_func($get);
        return json_decode($result);
    }

    private static function request_func($get)
    {
        $url = self::$gelios_url . '?login=' . self::$gelios_login . '&pass=' . self::$gelios_pass . '&svc=' . $get;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 90);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 0);
        $response = curl_exec($ch);
        if (curl_errno($ch)) {
            $response = curl_error($ch);
        }
        curl_close($ch);
        return $response;
    }

    final protected static function parse_params($params)
    {
        foreach ($params as $k => $v) {
            if (!in_array($k, static::$params)) {
                unset($params[$k]);
            }
        }
        return json_encode($params);
    }
}