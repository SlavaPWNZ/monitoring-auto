<?php

namespace Gelios\functions;

use Gelios\classes\execReportClass;

class geozoneClass
{
   public static function get($request){
       $json = json_decode($request['json'], true);
       $params = [
           "from" => intval($request['from']),
           "to" => intval($request['to']),
       ];
       $geozones = [];
       foreach ($json as $id => $geos){
           foreach ($geos as $geo){
               $params["id_unit"] = $id;
               $geo_str = $geo['lat'] . ', ' . $geo['lng'];
               $geozones[$id][$geo_str] = execReportClass::geozone($params, $geo_str);
           }
       }
       return $geozones;
   }
}