<?php

namespace Gelios\functions;

use Gelios\classes\execReportClass;
use Gelios\classes\execReportGroupClass;
use Gelios\geliosAPI;

class statsClass
{
   public static function get($request){
       $result = [];
       $params = [
           "from" => intval($request['from']),
           "to" => intval($request['to']),
       ];
       if ($request['id']){
           $params["id_unit"] = intval($request['id']);
           $stats = execReportClass::stats($params);
           $worktime= execReportClass::worktime($params);
           $result[] = (object) array_merge((array) $stats, (array) $worktime);
           return $result;
       }else{
           $params["id_group"] = geliosAPI::$gelios_main_group;
           $stats = execReportGroupClass::stats($params);
           $worktime= execReportGroupClass::worktime($params);
           foreach ($stats as $k => $v){
               if (!empty($worktime->$k)){
                   $result[$k] = (object) array_merge((array) $stats->$k, (array) $worktime->$k);
               }else{
                   $result[$k] = (object) array_merge((array) $stats->$k, ['time_start' => 0, 'time_end' => 0]);
               }
           }
           return $result;
       }
   }
}